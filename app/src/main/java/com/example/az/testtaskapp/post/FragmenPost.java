package com.example.az.testtaskapp.post;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.az.testtaskapp.R;
import com.example.az.testtaskapp.api.ApiService;
import com.example.az.testtaskapp.api.RetroClient;
import com.example.az.testtaskapp.api.RetroClientWeather;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmenPost extends Fragment {

    RecyclerView recyclerViewPost;
    ArrayList<Post> postArrayList;
    RVPostAdapter rvPostAdapter;
    Post post;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post, container, false);

        recyclerViewPost = view.findViewById(R.id.recyclerViewPost);
        recyclerViewPost.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewPost.setLayoutManager(layoutManager);
        postArrayList = new ArrayList<>();
        rvPostAdapter = new RVPostAdapter(getActivity(), postArrayList);
        recyclerViewPost.setAdapter(rvPostAdapter);
        Shared.list = new ArrayList<>();

        getPosts();

        return view;
    }


    private void getPosts() {
        ApiService api = RetroClient.getApiService();
        Call<ArrayList<Post>> call = api.getPosts();
        call.enqueue(new Callback<ArrayList<Post>>() {
            @Override
            public void onResponse(Call<ArrayList<Post>> call, Response<ArrayList<Post>> response) {
                if (response.isSuccessful()) {
                    Collections.shuffle(response.body());
                    List<Post> arrayList = response.body().subList(0, 10);
                    for (Post post2 : arrayList) {
                        postArrayList.add(post2);
                        Shared.list.add(post2.getId());
                    }
                    postExpense();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Post>> call, Throwable t) {

            }
        });
    }

    public void postExpense() {

        final ProgressDialog progressBar = new ProgressDialog(getActivity());
        progressBar.setCancelable(false);
        progressBar.setMessage("Загрузка...");
        progressBar.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    try {
                        postExpensesJson();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            progressBar.dismiss();
                                Collections.shuffle(postArrayList);
                                rvPostAdapter.notifyDataSetChanged();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Error! ", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                }
            }
        }).start();

    }


    private void postExpensesJson() throws Exception {

        ApiService api = RetroClientWeather.getApiServiceWhather();
        retrofit2.Call<Weather> call;

        City city = new City();
        ArrayList<City> cityArrayList = city.getCity();

        for (City c : cityArrayList) {
            String name = c.getName();
            String slug = c.getSlug();

            call = api.getWeather(slug);

            Response<Weather> response = call.execute();
            if (response.isSuccessful()) {
                String weather = response.body().getCurrent().getTemp_c();
                post = new Post();
                post.setTitle(name);
                post.setBody(weather);
                post.setWeather(true);
                postArrayList.add(post);
                Log.e("Responce : ", " - " + response.body().getCurrent().getTemp_c());

            } else {
                throw new Exception();
            }
        }
    }


}
