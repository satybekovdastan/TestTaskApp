package com.example.az.testtaskapp.album;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.az.testtaskapp.R;
import com.example.az.testtaskapp.photo.PhotosActivity;

import java.util.List;

public class RVAlbumAdapter extends RecyclerView.Adapter<RVAlbumAdapter.PersonViewHolder> {

    Context context;
    Album album;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView textTitle;

        PersonViewHolder(View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_title);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, PhotosActivity.class);
                    album = arrayList.get(getAdapterPosition());
                    intent.putExtra("albumId", album.getId());
                    Activity activity = (Activity) context;
                    activity.startActivity(intent);
                }
            });

        }

    }

    List<Album> arrayList;

    public RVAlbumAdapter(Context context, List<Album> listVse) {
        this.arrayList = listVse;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_album, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        album = new Album();
        album = arrayList.get(i);

        personViewHolder.textTitle.setText(album.getTitle());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

}