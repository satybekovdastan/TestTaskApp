package com.example.az.testtaskapp.photo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.az.testtaskapp.R;

import java.util.List;

public class RVPhotoAdapter extends RecyclerView.Adapter<RVPhotoAdapter.PersonViewHolder> {

    Context context;
    Photo photo;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        PersonViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_view);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FullPhotoActivity.class);
                    photo = arrayList.get(getAdapterPosition());
                    intent.putExtra("photo", photo);
                    Activity activity = (Activity) context;
                    activity.startActivity(intent);
                }
            });

        }

    }

    List<Photo> arrayList;

    public RVPhotoAdapter(Context context, List<Photo> listVse) {
        this.arrayList = listVse;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_photo, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        photo = new Photo();
        photo = arrayList.get(i);

        Glide.with(context).load(photo.getUrl()).into(personViewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

}