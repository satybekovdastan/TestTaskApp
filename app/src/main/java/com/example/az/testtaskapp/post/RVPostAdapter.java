package com.example.az.testtaskapp.post;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.az.testtaskapp.comment.CommentActivity;
import com.example.az.testtaskapp.R;

import java.util.ArrayList;

public class RVPostAdapter extends RecyclerView.Adapter<RVPostAdapter.PersonViewHolder> {

    Context context;
    Post post;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView textTitle,textBody;
        LinearLayout line;

        PersonViewHolder(View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_title);
            textBody = itemView.findViewById(R.id.text_body);
            line = itemView.findViewById(R.id.line);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    post = postArrayList.get(getAdapterPosition());
                    Intent intent;
                    if (post.isWeather()) {
                        intent = new Intent(context, BrowserActivity.class);
                    }else {
                        intent = new Intent(context, CommentActivity.class);
                    }
                    Activity activity = (Activity) context;
                    activity.startActivity(intent);
                }
            });

        }

    }

    ArrayList<Post> postArrayList;

    public RVPostAdapter(Context context, ArrayList<Post> listVse) {
        this.postArrayList = listVse;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_post, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        post = new Post();
        post = postArrayList.get(i);

        personViewHolder.textTitle.setText(post.getTitle());
        personViewHolder.textBody.setText(post.getBody());

        if (post.isWeather()){
            personViewHolder.textBody.setText(post.getBody()+"°");
            personViewHolder.line.setBackgroundResource(R.color.colorYellow);
        }else {
            personViewHolder.line.setBackgroundResource(0);
        }

    }

    @Override
    public int getItemCount() {
        return postArrayList.size();
    }

}