package com.example.az.testtaskapp.album;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.az.testtaskapp.DividerItemDecoration;
import com.example.az.testtaskapp.R;
import com.example.az.testtaskapp.api.ApiService;
import com.example.az.testtaskapp.api.RetroClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmenAlbum extends Fragment {

    RecyclerView recyclerViewAlbum;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_album, container, false);

        recyclerViewAlbum = view.findViewById(R.id.recyclerViewAlbum);

        recyclerViewAlbum.setHasFixedSize(true);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerViewAlbum.setLayoutManager(mGridLayoutManager);
        recyclerViewAlbum.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL));
        recyclerViewAlbum.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        getAlbum();

        return view;
    }


    private void getAlbum() {
        ApiService api = RetroClient.getApiService();
        Call<ArrayList<Album>> call = api.getAlbums();
        call.enqueue(new Callback<ArrayList<Album>>() {
            @Override
            public void onResponse(Call<ArrayList<Album>> call, Response<ArrayList<Album>> response) {
                if (response.isSuccessful()) {
                    Collections.shuffle(response.body());
                    List<Album> arrayList = response.body().subList(0, 10);
                    RVAlbumAdapter rvAlbumAdapter = new RVAlbumAdapter(getActivity(), arrayList);
                    recyclerViewAlbum.setAdapter(rvAlbumAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Album>> call, Throwable t) {

            }
        });
    }


}
