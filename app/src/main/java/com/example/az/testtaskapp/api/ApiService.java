package com.example.az.testtaskapp.api;

import com.example.az.testtaskapp.album.Album;
import com.example.az.testtaskapp.comment.Comment;
import com.example.az.testtaskapp.photo.Photo;
import com.example.az.testtaskapp.post.Post;
import com.example.az.testtaskapp.post.Weather;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("posts")
    Call<ArrayList<Post>> getPosts();

    @GET("albums")
    Call<ArrayList<Album>> getAlbums();

    @GET("comments?postId=/tasks")
    Call<ArrayList<Comment>> getCommentPostId(@Query("postId") int postId);

    @GET("photos?albumId=/tasks")
    Call<ArrayList<Photo>> getAlbumPhotos(@Query("albumId") int albumId);

    @GET("current.json?key=f2df1c269578411c9c4131606180607&q=/tasks")
    Call<Weather> getWeather(@Query("q") String slug);


}