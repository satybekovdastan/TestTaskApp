package com.example.az.testtaskapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.az.testtaskapp.album.FragmenAlbum;
import com.example.az.testtaskapp.post.FragmenPost;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView navigation;
    private int mSelectedItem;
    private static final String SELECTED_ITEM = "arg_selected_item";
    private Fragment fragment;
    int itemSelect = 2;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Post");
        navigation = findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectFragment(item);
                return true;
            }
        });

        MenuItem selectedItem;
        if (savedInstanceState != null) {
            mSelectedItem = savedInstanceState.getInt(SELECTED_ITEM, 0);
            selectedItem = navigation.getMenu().findItem(mSelectedItem);
        } else {
            selectedItem = navigation.getMenu().getItem(0);
        }
        try {
            selectFragment(selectedItem);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void selectFragment(MenuItem item) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (item.getItemId()) {
            case R.id.navigation_post:
                if (itemSelect != 0) {
                    toolbar.setTitle("Post");
                    getSupportFragmentManager().popBackStack();
                    fragment = new FragmenPost();
                    ft.replace(R.id.content, fragment);
                    ft.commit();
                    itemSelect = 0;
                }
                return;
            case R.id.navigation_album:
                if (itemSelect != 1) {
                    toolbar.setTitle("Album");
                    getSupportFragmentManager().popBackStack();
                    fragment = new FragmenAlbum();
                    ft.replace(R.id.content, fragment);
                    ft.commit();
                    itemSelect = 1;
                }
                return;
        }

        mSelectedItem = item.getItemId();
        for (int i = 0; i < navigation.getMenu().size(); i++) {
            MenuItem menuItem = navigation.getMenu().getItem(i);
            menuItem.setChecked(menuItem.getItemId() == item.getItemId());
        }

    }
}
