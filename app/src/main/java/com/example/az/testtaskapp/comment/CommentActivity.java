package com.example.az.testtaskapp.comment;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.az.testtaskapp.R;
import com.example.az.testtaskapp.post.Shared;
import com.example.az.testtaskapp.api.ApiService;
import com.example.az.testtaskapp.api.RetroClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class CommentActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<Comment> commentArrayList;
    RVCommentAdapter rvCommentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        Toolbar toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Comment");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        commentArrayList = new ArrayList<>();
        rvCommentAdapter = new RVCommentAdapter(CommentActivity.this, commentArrayList);
        recyclerView.setAdapter(rvCommentAdapter);

        initComment();

    }


    public void initComment() {

        final ProgressDialog progressBar = new ProgressDialog(CommentActivity.this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Загрузка...");
        progressBar.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getComments();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.dismiss();
                            rvCommentAdapter.notifyDataSetChanged();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.dismiss();
                            Toast.makeText(CommentActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        }).start();

    }


    private void getComments() throws Exception {

        ApiService api = RetroClient.getApiService();
        retrofit2.Call<ArrayList<Comment>> call;

        List<Integer> listId = Shared.list;
        for (int i = 0; i < listId.size(); i++) {
            call = api.getCommentPostId(listId.get(i));

            Response<ArrayList<Comment>> response = call.execute();
            if (response.isSuccessful()) {
                commentArrayList.add(response.body().get(0));
            } else {
                throw new Exception();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
