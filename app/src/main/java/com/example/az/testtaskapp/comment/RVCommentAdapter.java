package com.example.az.testtaskapp.comment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.az.testtaskapp.R;

import java.util.ArrayList;

public class RVCommentAdapter extends RecyclerView.Adapter<RVCommentAdapter.PersonViewHolder> {

    Context context;
    Comment comment;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView textName,textBody, textEmail;

        PersonViewHolder(View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.text_name);
            textBody = itemView.findViewById(R.id.text_body);
            textEmail = itemView.findViewById(R.id.text_email);
        }

    }

    ArrayList<Comment> commentArrayList;

    public RVCommentAdapter(Context context, ArrayList<Comment> listVse) {
        this.commentArrayList = listVse;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_comment, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        comment = new Comment();
        comment = commentArrayList.get(i);

        personViewHolder.textBody.setText(comment.getBody());
        personViewHolder.textName.setText(comment.getName());
        personViewHolder.textEmail.setText(comment.getEmail());

    }

    @Override
    public int getItemCount() {
        return commentArrayList.size();
    }

}