package com.example.az.testtaskapp.post;

import java.util.ArrayList;

public class City {
    String name;
    String slug;

    public void setName(String name) {
        this.name = name;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public ArrayList<City> getCity(){
        ArrayList arrayList = new ArrayList();
        City city =new City();
        city.setName("Bishkek");
        city.setSlug("Bishkek");
        arrayList.add(city);

        city =new City();
        city.setName("Osh");
        city.setSlug("Osh Oblasty");
        arrayList.add(city);

        city =new City();
        city.setName("Talas");
        city.setSlug("Talas");
        arrayList.add(city);

        city =new City();
        city.setName("Чолпон-Ата");
        city.setSlug("Cholpon-Ata");
        arrayList.add(city);

        city =new City();
        city.setName("Нарын");
        city.setSlug("Naryn");
        arrayList.add(city);

        return arrayList;
    }
}
